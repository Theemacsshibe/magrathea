# magrathea: chaotic-neutral web security for Hunchentoot

- Bored of people scanning your website for vulnerabilities for have?
- Want to do something about skiddies?
- Hate scrapers?

Magrathea is just for you! It uses state of the art
[zip bombs](https://en.wikipedia.org/wiki/Zip_bomb), with only a sketchy URL
list required for normal use. 

## Installation

Clone this repository somewhere Quicklisp (or asdf) will find it:

```
$ cd ~/quicklisp/local-projects
$ git clone https://gitlab.com/Theemacsshibe/magrathea
```

Load it:

```
CL-USER> (ql:quickload :magrathea)
To load "magrathea":
  Load 1 ASDF system:
    magrathea
; Loading "magrathea"
......
```

## Usage

Use `MAGRATHEA:MAGRATHEA-ACCEPTOR` in place of `HUNCHENTOOT:EASY-ACCEPTOR`.

For example, `(hunchentoot:start (make-instance 'magrathea:magrathea-acceptor :port 8080))`
runs the "easy teen-age New York" Hunchentoot demo on port 8080.

Some extra initargs are provided:

- `SENSITIVITY`: how many sketchy requests must be sent before Magrathea triggers.
- `WARN-IF-ABOUT-TO-BLOW`: if non-NIL, sends the warning message `*WARNING-TEXT*`
  before Magrathea zip-bombs the user. (It's generally polite to do so.)

Several accessors are also provided:

- `MAGRATHEA-SHADY-REQUESTS`: a hash table with sketchy request counts.
- `MAGRATHEA-LAST-REQUEST-TIMES`: a hash table with the last request times, in
  internal time format.
- `MAGRATHEA-SENSITIVITY`: the SENSITIVITY initarg of the acceptor.
- `MAGRATHEA-WARN-IF-ABOUT-TO-BLOW`: the WARN-IF-ABOUT-TO-BLOW initarg of the acceptor.

Some variables may also be of interest:

- `*SKETCHY-PATTERNS*`: a list of regexp patterns and functions which should match or return
  non-NIL if they are sketchy. Functions are simply passed the request path, and are provided
  in case you need something more high-tech than just regexps.
- `*IGNORE-AFTER-TIME*`: how long (in internal time units) before Magrathea forgets about
  sketchy requests.
- `*WARNING-TEXT*`: the text sent before clients of acceptors with `WARN-IF-ABOUT-TO-BLOW`
  set to non-NIL are zip-bombed.
- `*FULL-ACCEPTOR-GC-REQUESTS*`: how many requests should be made between cleanings of
  history tables of acceptors.
- `*PAYLOAD-TYPE*`: the compression type used by the payload. `gzip` is commonly accepted, so
  that is used most.
- `*PAYLOAD-LENGTH*`: the real length of the compressed payload.
- `*PAYLOAD*`: the payload as a vector of octets. Defaults to loading `data/payload.gz` in the
  source directory.

## Bugs

- Gzip is kinda big, and ideally we'd like to send over a bzip2 or Brotli file.
  Can this be done? Testing with brotli suggests no one requests Brotli, and
  only Firefox will die decompressing it.
- We send over a gzipped file even if the client didn't ask for gzip. I suppose
  that's not too bad since the client might be stupid enough to try to
  decompress anyway (eg Firefox), but still it could be more sneaky in that
  regard.

## Licensing

This library is licensed under the 2-clause BSD license, the same as
Hunchentoot.

Zip bombs were taken from
[bones-codes/bombs](https://github.com/bones-codes/bombs), which are licensed
under the GNU Affero General Public license. How does that work for
zip bombs then? Hopefully I don't have to send 30GB files of zeros or whatever
the contents of the archives are.

The idea for this library was taken from this blog post:
https://blog.haschek.at/2017/how-to-defend-your-website-with-zip-bombs.html