(defpackage magrathea
  (:use :cl :hunchentoot :cl-ppcre)
  (:export :magrathea-acceptor
           :magrathea-shady-requests
           :magrathea-last-request-times
           :magrathea-sensitivity
           :magrathea-warn-if-about-to-blow
           :*payload-type*
           :*payload*
           :*ignore-after-time*
           :*warning-text*
           :*sketchy-patterns*))
